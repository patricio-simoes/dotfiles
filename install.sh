#!/usr/bin/env bash

DOTFILES="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

# This script creates symbolic links to the .config folder.
# This allows us to easily manage dotfiles using git.

declare -A links=(
    ["$DOTFILES/.bash_aliases"]="$HOME/.bash_aliases"
    ["$DOTFILES/alacritty/"]="$HOME/.config/alacritty"
    ["$DOTFILES/fastfetch/"]="$HOME/.config/fastfetch"
    ["$DOTFILES/hypr/"]="$HOME/.config/hypr"
    ["$DOTFILES/MangoHud/"]="$HOME/.config/MangoHud"
    ["$DOTFILES/starship.toml"]="$HOME/.config/"
    ["$DOTFILES/waybar/"]="$HOME/.config/waybar"
    ["$DOTFILES/wofi/"]="$HOME/.config/wofi"
)

declare -A identifiers=(
    ["$DOTFILES/.bash_aliases"]="Bash Aliases"
    ["$DOTFILES/alacritty/"]="Alacritty"
    ["$DOTFILES/fastfetch/"]="Fastfetch"
    ["$DOTFILES/hypr/"]="Hyprland"
    ["$DOTFILES/MangoHud/"]="MangoHud"
    ["$DOTFILES/starship.toml"]="Starship"
    ["$DOTFILES/waybar/"]="Waybar"
    ["$DOTFILES/wofi/"]="Wofi"
)

for src in "${!links[@]}"; do
    target="${links[$src]}"
    identifier="${identifiers[$src]}"

    read -p "Do you want to create a symlink for $identifier? (Y/N): " choice
    if [[ "$choice" =~ ^[Yy]$ ]]; then
        if [[ "$target" == "$HOME/.config/" ]]; then
            echo "Refusing to remove the .config directory."
            continue
        elif [ -e "$target" ]; then
            rm -rf "$target"
            echo "Removed existing target: $target"
        fi

        mkdir -p "$(dirname "$target")"

        ln -s "$src" "$target"
        echo "Created symlink: $target -> $src"
    else
        :
    fi
done
