# MangoHud

Important notes on this config:

- FPS are capped at 120 FPS;
- Shift + F12 Hides/Shows MangoHud.

The following will be placed on the left side of your screen, vertically aligned to the center:

![Preview](./preview.png)