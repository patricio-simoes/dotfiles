# Neofetch

To display neofetch, do one of the following:

- Type 'neofetch' on your terminal;
- Add 'neofetch' to your bash file.

**Note:** This config has become deprecated in favor of fastfetch since the development for neofetch has stopped.

![Preview](./preview.png)