# Fastfetch

To display fastfetch, do one of the following:

- Type 'fastfetch' on your terminal;
- Add 'fastfetch' to your bash file.

![Preview](./preview.png)
